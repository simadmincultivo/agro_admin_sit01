﻿#pragma warning disable 0649
using NSAvancedUI;
using NSBoxMessage;
using NSTraduccionIdiomas;
using UnityEngine.SceneManagement;

namespace NSInterfaz
{
    public class PanelBotonesGeneralesSituacion : AbstractSingletonPanelUIAnimation<PanelBotonesGeneralesSituacion>
    {
        
        public void OnButtonReiniciarSituacion()
        {
            BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("mensajeReiniciarPractica"), DiccionarioIdiomas.Instance.Traducir("TextCancelar"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), RestartSituationConfirm);
        }

        public void OnButtonReturnBackSituation()
        {
            BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("mensajeAbandonarPractica"), DiccionarioIdiomas.Instance.Traducir("TextCancelar"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), ConfirmReturnBackSituation);
        }

        private void RestartSituationConfirm()
        {
            SceneManager.LoadScene(1);
        }

        private void ConfirmReturnBackSituation()
        {
            SceneManager.LoadScene(0);
        }
    }
}