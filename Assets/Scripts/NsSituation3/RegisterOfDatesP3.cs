﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using NSInterfaz;
using NSInterfazAvanzada;
using NSBoxMessage;
using NSTraduccionIdiomas;
using NSCalificacionSituacion;

public class RegisterOfDatesP3 : MonoBehaviour
{
    [Header("Variable calificación registro de datos")]
    public float calificacion2;

    [Header("Referencia a Scripts")]
    [SerializeField] private InstantiateController myInstanceController;
    [SerializeField] private ValuesOfProductsManagerP3 myValuesProducts;
    [SerializeField] private CalculoCantidadProductosP3 myCalculoCantidadProductos;

    [Header("Objetos en escena")]
    [SerializeField] private GameObject[] xInFields;

    [Header("Porcentaje error")]
    [SerializeField] private float valueError;

    [Header("Variables para intentos")]
    [SerializeField] private TextMeshProUGUI AttemptsText;
    [SerializeField] private SOSessionData soSessionData;
    private int countAttempts = 1;

    [SerializeField] private TMP_InputField[] fieldsRegisterDates3;
    private float sumTotalDates3;
    private float sumStorage;
    private float sumSelected;
    [SerializeField] private float sumCostFijosP3;
    [SerializeField] private float sumCostVariablesP3;
    [SerializeField] private List<float> listValuesToCompare = new List<float>();
    private bool canContinius;

    public void ButtonCheckRegister3()
    {
        SumPriceOfProductsInBuilds();
        CheckValuesInRegisterAndTable();
    }

    void SumPriceOfProductsInBuilds()
    {
        listValuesToCompare.Clear();
        sumStorage = 0;
        sumSelected = 0;
        sumCostFijosP3 = 0;
        sumCostVariablesP3 = 0;

        listValuesToCompare.Add(myValuesProducts.cantidadKilosProducidosCultivo);

        listValuesToCompare.Add(myInstanceController.valueCostoGreenHouseP3);

        for(int i = 0; i < myInstanceController.listStorageP3.Count; i++)
        {
            sumStorage += myInstanceController.listStorageP3[i];
        }
        listValuesToCompare.Add(sumStorage);

        for(int i = 0; i < myInstanceController.listSelected.Count; i++)
        {
            sumSelected += myInstanceController.listSelected[i];
        }
        listValuesToCompare.Add(sumSelected);

        for(int i = 0; i < myInstanceController.listCostosFijosP3.Count; i++)
        {
            sumCostFijosP3 += myInstanceController.listCostosFijosP3[i];
        }
        listValuesToCompare.Add(sumCostFijosP3);

        for(int i = 0; i < myInstanceController.listCostosVariablesP3.Count; i++)
        {
            sumCostVariablesP3 += myInstanceController.listCostosVariablesP3[i];
        }
        listValuesToCompare.Add(sumCostVariablesP3);
    }

    void CheckValuesInRegisterAndTable()
    {
        int countErrors = 0;
        calificacion2 = 0;
        for(int i = 0; i < fieldsRegisterDates3.Length; i++)
        {
            if(fieldsRegisterDates3[i].text != "")
            {
                float valueUser = float.Parse(fieldsRegisterDates3[i].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                float valueCompare = listValuesToCompare[i];
                if(valueUser >= (valueCompare / valueError) && valueUser <= (valueCompare * valueError))
                {
                    xInFields[i].SetActive(false);
                    calificacion2 += 1f / fieldsRegisterDates3.Length;
                }
                else
                {
                    xInFields[i].SetActive(true);
                    countErrors++;
                }
            }
            else 
            {
                xInFields[i].SetActive(true);
                countErrors++;
            }
        }

        if(countErrors > 0)
        {
            PanelMensajeIncorrecto.Instance.ShowPanel();
            canContinius = false;
            countAttemptsMetod();
        }
        else
        {
            BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("mensajeDatosIngresadosCorrectos"), DiccionarioIdiomas.Instance.Traducir("TextCancelar"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), null, null);
            canContinius = true;
        }
    }

    public void ButtonReport()
    {
        if(canContinius)
        {
            PanelRegistroDatos.Instance.ShowPanel(false);
            PanelInterfazEvaluacion.Instance.ShowPanel();
        }
        else
        {
            BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("mensajeDatosIngresadosIncorrectos"), DiccionarioIdiomas.Instance.Traducir("TextCancelar"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), ButtonAceptar, null);
        }
    }

    void ButtonAceptar()
    {
        PanelRegistroDatos.Instance.ShowPanel(false);
        PanelInterfazEvaluacion.Instance.ShowPanel();
        countAttemptsMetod();
    }

    public void countAttemptsMetod()
    {
        countAttempts += 1;
        AttemptsText.text = countAttempts.ToString();
        soSessionData.quantityAttempts = (byte) countAttempts;
    }
}
