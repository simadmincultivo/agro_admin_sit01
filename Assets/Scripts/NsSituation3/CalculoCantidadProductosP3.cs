﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CalculoCantidadProductosP3 : MonoBehaviour
{
    [Header("Variable calificación ventanas individuales")]
    public float calificacion1;

    [SerializeField] private ValuesOfProductsManagerP3 myValuesProductos;

    [Header("objectos y datos de invernadero")]
    [SerializeField] private TMP_Dropdown dropInvernadero;
    private int cantidadJornalesInvernadero;

    [Header("Objetos y datos de centro de acopio")]
    [SerializeField] private TMP_InputField fieldCantidadCanastillas;
    [SerializeField] private TMP_Dropdown dropRecursoHumanoAlmacenamiento;
    private int cantidadJornalesAlmacenamiento;

    [Header("Objetos y datos de selección y empaque")]
    [SerializeField] private TMP_InputField fieldCantidadCanastillas2;
    [SerializeField] private TMP_InputField fieldBalanzas;
    [SerializeField] private TMP_Dropdown[] fieldsLimpiezaSeleccion;
    private int cantidadJornalesLimpieza;
    private int cantidadJornalesSeleccion;

    [Header("Objetos y datos de administración")]
    [SerializeField] private TMP_Dropdown[] fieldAdmin;
    private int cantidadJornalesAdmin;
    private int cantidadJornalesSecretaria;

    private float valueToDivideCalification = 1f / 7;

    [Header("Valor porcentaje error")]
    [SerializeField] private float valueError;

    private bool duplicateValue = false;
    private bool duplicateValue1 = false;
    private bool duplicateValue2 = false;
    private bool duplicateValue3 = false;
    private bool duplicateValue4 = false;
    private bool duplicateValue5 = false;
    private bool duplicateValue6 = false;

    public void CalcularJornalesInvernadero()
    {
        cantidadJornalesInvernadero = CalculateJornales(2);
        if(int.Parse(dropInvernadero.captionText.text) == cantidadJornalesInvernadero)
        {
            if(!duplicateValue)
            {
                calificacion1 += valueToDivideCalification;
                duplicateValue = true;
            }
        }
        else
        {
            duplicateValue = false;
        }
    }

    public void VerificarCantidadCanastillas()
    {
        float aux = myValuesProductos.cantidadCanastillasAcopio;
        int valueUser = int.Parse(fieldCantidadCanastillas.text);
        if(valueUser >= (aux / valueError) && valueUser <= (aux * valueError))
        {
            if(!duplicateValue1)
            {
                calificacion1 += valueToDivideCalification;
                duplicateValue1 = true;
            }
        }
        else
        {
            duplicateValue1 = false;
        }
    }

    public void VerificarJornalesAlmacenamiento()
    {
        cantidadJornalesAlmacenamiento = CalculateJornales(2);
        if(int.Parse(dropRecursoHumanoAlmacenamiento.captionText.text) == cantidadJornalesAlmacenamiento)
        {
            if(!duplicateValue2)
            {
                calificacion1 += valueToDivideCalification;
                duplicateValue2 = true;
            }
        }
        else
        {
            duplicateValue2 = false;
        }
    }

    public void VerificarCantidadCanastillas2()
    {
        float aux = myValuesProductos.cantidadCanastillas;
        int valueUser = int.Parse(fieldCantidadCanastillas2.text);
        if(valueUser >= (aux / valueError) && valueUser <= (aux * valueError))
        {
            if(!duplicateValue3)
            {
                calificacion1 += valueToDivideCalification;
                duplicateValue3 = true;
            }
        }
        else
        {
            duplicateValue3 = false;
        }
    }

    public void VerificarCantidadBalanzas()
    {
        float aux = 1f;
        int valueUser = int.Parse(fieldBalanzas.text);
        if(valueUser >= (aux / valueError) && valueUser <= (aux * valueError))
        {
            if(!duplicateValue4)
            {
                calificacion1 += valueToDivideCalification;
                duplicateValue4 = true;
            }
        }
        else
        {
            duplicateValue4 = false;
        }
    }

    public void VerificarJornalesLimpiezaSeleccion()
    {
        cantidadJornalesLimpieza = CalculateJornales(3);
        cantidadJornalesSeleccion = CalculateJornales(4);
        if(int.Parse(fieldsLimpiezaSeleccion[0].captionText.text) == cantidadJornalesLimpieza && int.Parse(fieldsLimpiezaSeleccion[1].captionText.text) == cantidadJornalesSeleccion)
        {
            if(!duplicateValue5)
            {
                calificacion1 += valueToDivideCalification;
                duplicateValue5 = true;
            }
        }
        else
        {
            duplicateValue5 = false;
        }
    }

    public void VerificarJornalesAdministradores()
    {
        cantidadJornalesAdmin = 20;
        cantidadJornalesSecretaria = 20;
        if(int.Parse(fieldAdmin[0].captionText.text) == cantidadJornalesAdmin && int.Parse(fieldAdmin[1].captionText.text) == cantidadJornalesSecretaria)
        {
            if(!duplicateValue6)
            {
                calificacion1 += valueToDivideCalification;
                duplicateValue6 = true;
            }
        }
        else
        {
            duplicateValue6 = false;
        }
    }

    private int CalculateJornales(int aplications)
    {
        int value = (20 / aplications);
        return value;
    }
}
