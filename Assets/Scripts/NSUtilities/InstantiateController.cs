﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InstantiateController : MonoBehaviour
{
    public List<float> sumTotal;

    public float countToView = 0;

    [SerializeField] private GameObject prefabToTablet;
    [SerializeField] private GameObject parentForProducts;
    [SerializeField] private TextMeshProUGUI textCountTotal;
    public int numProduct = 0;

    public List<float> listGreenHouse;
    public List<float> listHangar;
    public List<float> listGerminator;
    public List<float> listCellar;
    public List<float> listHangarP2;
    public List<float> listGreenHouseP2;
    public List<float> listGerminatorP2;
    public float valueCostoGreenHouseP3;
    public List<float> listStorageP3;
    public List<float> listSelected;
    public List<float> listCostosFijosP4;
    public List<float> listCostosVariablesP4;
    public List<float> listCostosFijosP3;
    public List<float> listCostosVariablesP3;
    
    private bool aux;

    public void InstanceProductInTablet(string zone, string id, string nameProduc, string unity, float countT, int price)
    {
        numProduct = 0;

        for(int i = 0; i < parentForProducts.transform.childCount; i++)
        {
            if(parentForProducts.transform.GetChild(i).transform.name.Equals(id))
            {
                float priceToRemove = 0;
                Destroy(parentForProducts.transform.GetChild(i).gameObject);
                float lastPrice = float.Parse(parentForProducts.transform.GetChild(i).GetChild(4).GetComponent<TextMeshProUGUI>().text);
                float lastCount = float.Parse(parentForProducts.transform.GetChild(i).GetChild(3).GetComponent<TextMeshProUGUI>().text);
                priceToRemove = lastPrice * lastCount;
                RemovePriceToCorrectZone(zone, priceToRemove);
                sumTotal.Remove(priceToRemove);
                Debug.Log(priceToRemove);
            }
        }

        if(id == "LuzAmbiente" && price == 0)
        {
            StartCoroutine(SetOrderNumber());
            return;
        }

        float priceCount = 0;
        var tmpBlankFile =  Instantiate(prefabToTablet, parentForProducts.transform);
        
        tmpBlankFile.name = id;
        tmpBlankFile.tag = zone;        

        tmpBlankFile.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = nameProduc;
        tmpBlankFile.transform.GetChild(2).GetComponent<TEXDraw>().text = unity;
        tmpBlankFile.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = countT.ToString();
        tmpBlankFile.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = price.ToString("00.00");
        
        priceCount = countT * price;
        sumTotal.Add(priceCount);
        RealiceSumOfPrice();
        
        AddPriceToCorrectZone(zone, priceCount);
    }

    IEnumerator SetOrderNumber()
    {
        yield return new WaitForEndOfFrame();
        numProduct = 0;
        for(int i = 0; i < parentForProducts.transform.childCount; i++)
        {
            numProduct++;
            parentForProducts.transform.GetChild(i).transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = numProduct.ToString();
        }
    }

    public void RealiceSumOfPrice()
    {
        countToView = 0;
        for(int i = 0; i < sumTotal.Count; i++)
        {
            countToView += sumTotal[i];
        }
        textCountTotal.text = countToView.ToString();
        StartCoroutine(SetOrderNumber());
    }

    public void RemovePriceToCorrectZone(string zone, float price)
    {
        if(zone == "GreenHouse")
            listGreenHouse.Remove(price);
        else if(zone == "Hangar")
            listHangar.Remove(price);
        else if(zone == "Germinator")
            listGerminator.Remove(price);
        else if(zone == "Cellar")
            listCellar.Remove(price);
        else if(zone == "HangarP2")
            listHangarP2.Remove(price);
        else if(zone == "GreenHouseP2")
            listGreenHouseP2.Remove(price);
        else if(zone == "GerminatorP2")
            listGerminatorP2.Remove(price);
        else if(zone == "GreenHouseP3")
        {
            valueCostoGreenHouseP3 = 0;
            listCostosVariablesP3.Remove(price);
        }           
        else if(zone == "Storage")
        {
            listStorageP3.Remove(price);
            listCostosFijosP3.Remove(price);
        }
        else if(zone == "Storage2")
        {
            listStorageP3.Remove(price);
            listCostosVariablesP3.Remove(price);
        }
        else if(zone == "Selected")
        {
            listSelected.Remove(price);
            listCostosFijosP3.Remove(price);
        }
        else if(zone == "Selected2")
        {
            listSelected.Remove(price);
            listCostosVariablesP3.Remove(price);
        }
        else if(zone == "Admin")
        {
            listCostosFijosP3.Remove(price);
        }
        else if(zone == "CostFijo")
            listCostosFijosP4.Remove(price);
        else if(zone == "CostVariable")
            listCostosVariablesP4.Remove(price);
    }

    void AddPriceToCorrectZone(string zone, float price)
    {
        if(zone == "GreenHouse")
            listGreenHouse.Add(price);
        else if(zone == "Hangar")
            listHangar.Add(price);
        else if(zone == "Germinator")
            listGerminator.Add(0);
        else if(zone == "Cellar")
            listCellar.Add(price);
        else if(zone == "HangarP2")
            listHangarP2.Add(price);
        else if(zone == "GreenHouseP2")
            listGreenHouseP2.Add(0);
        else if(zone == "GerminatorP2")
            listGerminatorP2.Add(price);
        else if(zone == "GreenHouseP3")
        {
            valueCostoGreenHouseP3 = price;
            listCostosVariablesP3.Add(price);
        }
        else if(zone == "Storage")
        {
            listStorageP3.Add(price);
            listCostosFijosP3.Add(price);
        }
        else if(zone == "Storage2")
        {
            listStorageP3.Add(price);
            listCostosVariablesP3.Add(price);
        }
        else if(zone == "Selected")
        {
            listSelected.Add(price);
            listCostosFijosP3.Add(price);
        }
        else if(zone == "Selected2")
        {
            listSelected.Add(price);
            listCostosVariablesP3.Add(price);
        }
        else if(zone == "Admin")
        {
            listCostosFijosP3.Add(price);
        }
        else if(zone == "CostFijo")
            listCostosFijosP4.Add(price);
        else if(zone == "CostVariable")
            listCostosVariablesP4.Add(price);
    }
}
