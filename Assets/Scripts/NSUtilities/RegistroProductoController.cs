﻿using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine;
using NSBoxMessage;
using TMPro;
using UnityEngine.SceneManagement;
using NSTraduccionIdiomas;

public class RegistroProductoController : MonoBehaviour, IPointerClickHandler
{    
    private InstantiateController myInstantiateControll;
    private StructuresControll1 myStructuresControll1;

    private int numProduct;

    void Start()
    {
        myInstantiateControll = FindObjectOfType<InstantiateController>();
        myStructuresControll1 = FindObjectOfType<StructuresControll1>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("TextEliminarProdcuto"), DiccionarioIdiomas.Instance.Traducir("TextCancelar"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), ButtonConfirm, null);
        //int id = int.Parse(transform.GetChild(0).GetComponent<TextMeshProUGUI>().text);
    }

    public void ButtonConfirm()
    {
        float priceToRemove = 0;
        float lastPrice = float.Parse(transform.GetChild(4).GetComponent<TextMeshProUGUI>().text);
        float lastCount = float.Parse(transform.GetChild(3).GetComponent<TextMeshProUGUI>().text);
        priceToRemove = lastPrice * lastCount;
        myInstantiateControll.RemovePriceToCorrectZone(transform.tag, priceToRemove);
        myInstantiateControll.sumTotal.Remove(priceToRemove);
        myInstantiateControll.RealiceSumOfPrice();
        DeleteObjectInScene();
        Destroy(this.gameObject);
    }

    public void DeleteObjectInScene()
    {
        GameObject objInScene = GameObject.Find(transform.name);
        GameObject parentObjInScene;

        if(SceneManager.GetActiveScene().name == "Practica1")
        {
            if(myStructuresControll1 != null)
            {
                if(myStructuresControll1.analyzerSoil)
                {
                    myStructuresControll1.analyzerSoil = false;
                }
            }
        }        

        if(objInScene != null)
        {
            if(objInScene.transform.parent != null && objInScene.transform.name != "Luzcapilla")
            {
                parentObjInScene = objInScene.transform.parent.gameObject;
                if(parentObjInScene.name != "Content")
                {
                    Debug.Log(objInScene.transform.parent.name);
                    if(parentObjInScene.transform.childCount > 0)
                    {
                        for(int i = 0; i < parentObjInScene.transform.childCount; i++)
                        {
                            if(parentObjInScene.transform.GetChild(i).gameObject.activeSelf)
                            {
                                parentObjInScene.transform.GetChild(i).gameObject.SetActive(false);
                            }
                        }
                    }
                }
            }
            else
            {
                objInScene.SetActive(false);
            }    
        }
    }
}
