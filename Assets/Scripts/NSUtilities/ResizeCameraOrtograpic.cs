using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace NSUtilities
{
    [ExecuteInEditMode]
    public class ResizeCameraOrtograpic : MonoBehaviour
    {
        #region members
        
        private Camera refCamera;

        [SerializeField] private int minWidthWindow;
        
        [SerializeField] private int minHeightWindow;
        
        [Header("Aspect Ratio Config")] 
        
        [SerializeField, Tooltip("Ratio base objetivo de la pantalla, 16/9, 16/10 etc")] private float ratioBase;
        
        [SerializeField, Tooltip("Ratio maximo de escalado de la camara, ratios por encima de este no sera tenidos en cuenta, el valor debe ser minimo igual al del objetivo")] private float maxRatioSupported;
        
        [SerializeField, Tooltip("Orthographic size minima de la camara, la camara no se acerca mas de este valor")] private float minOrthographicSizeCamera;
        
        [SerializeField, Tooltip("Orthographic size adicional que la camara puede tener, para que la camara pueda abarcar todo el escenario")] private float orthographicSizeCameraAdd;

        private float previusRatio;
        #endregion

        #region Mono behaviour
        
        private void Awake()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void Start()
        {
            CalculateSize();
        }

        private void LateUpdate()
        {
            CalculateOrthograpicSize();
        }

        #endregion
        
        private void OnSceneLoaded(Scene argScene, LoadSceneMode argLoadSceneMode)
        {
            previusRatio = -100000;
        }

        private void CalculateSize()
        {
            var tmpScreenWidth = Screen.width;
            var tmpScreenHeight = Screen.height;
            tmpScreenWidth = Mathf.Clamp(tmpScreenWidth, minWidthWindow, 1920);
            tmpScreenHeight = Mathf.Clamp(tmpScreenHeight, minHeightWindow, 1080);
            Screen.SetResolution(tmpScreenWidth, tmpScreenHeight, Screen.fullScreen, 30);
        }

        private void CalculateOrthograpicSize()
        {
            var tmpRatioNew = (float) Screen.width / (float) Screen.height;
            var tmpDifference = tmpRatioNew - ratioBase;
            if (tmpDifference > 0f)
            {
                if (Math.Abs(previusRatio - tmpRatioNew) > 0.000005f)
                {
                    refCamera = Camera.main;

                    if(refCamera != null)
                    {
                        refCamera.orthographicSize = minOrthographicSizeCamera + Mathf.InverseLerp(maxRatioSupported , ratioBase, tmpRatioNew) * orthographicSizeCameraAdd;
                        previusRatio = tmpRatioNew;
                    }
                }
            }
        }
    }
}