﻿
using System;
using UnityEngine;
using UnityEngine.UI;

namespace NSUtilities
{
    public class RebuildLayoutRectTransform : MonoBehaviour
    {
        #region MonoBehaviour

        private void OnEnable()
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
        }
        #endregion
    }
}