﻿using TMPro;
using UnityEngine;
using NSScriptableValues;
using System.Collections;
using NSInterfazAvanzada;
using System;

public class CalificacionSituacion1 : MonoBehaviour
{
    [Header("Referencias a Scripts")]
    [SerializeField] private RegisterOfDates myRegisterDates1;
    [SerializeField] private InstantiateController myInstanciateController;

    [Header("Referencia a Scriptable Objetc")]
    [SerializeField] private ValueFloat valCalificationRegisterData;
    [SerializeField] private ValueFloat valCalificationTablet;


    public void CheckCalificationFinal()
    {
        valCalificationRegisterData.Value = myRegisterDates1.calification1;

        if(myInstanciateController.countToView <= myRegisterDates1.valorParaRealizacion)
            valCalificationTablet.Value = 1f;
        else
            valCalificationTablet.Value = 0;
    }
}