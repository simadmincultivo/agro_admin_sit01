﻿using TMPro;

namespace NSTraduccionIdiomas
{
    public class TextForTranslate
    {
        public int instanceID;
        
        public TextMeshProUGUI textMeshProUgui;

        public TextTraductor textTraductor;

        public TextForTranslate(TextMeshProUGUI argTextMeshProUGUI)
        {
            instanceID = argTextMeshProUGUI.GetInstanceID();
            textMeshProUgui = argTextMeshProUGUI;
            textTraductor = argTextMeshProUGUI.GetComponent<TextTraductor>();
        }
    }
}