#pragma warning disable 0649
using NSScriptableValues;
using UnityEngine;
using UnityEngine.UI;

namespace NSScriptableValues
{
    public class SetScriptableValueToText : MonoBehaviour
    {
        [SerializeField] private ValueInt quantityValue;

        [SerializeField] private Text textQuantity;

        [Header("Complementary text")] [SerializeField]
        private bool setTextBeforeQuantity;

        [SerializeField] private string textComplementary;
        
        private void OnEnable()
        {
            quantityValue.OnValueChanged.AddListener(SetValueToText);
            quantityValue.Value = 1000;
        }

        private void OnDisable()
        {
            quantityValue.OnValueChanged.RemoveListener(SetValueToText);
        }

        private void SetValueToText()
        {
            if (setTextBeforeQuantity)
                textQuantity.text = quantityValue + textComplementary;
            else
                textQuantity.text = textComplementary + quantityValue;
        }
    }
}