using UnityEngine;

namespace NSScriptableValues
{
    [CreateAssetMenu(fileName = "Transform", menuName = "NSScriptableValue/Transform", order = 0)]
    public class ValueTransform : AbstractScriptableValue<Transform>
    {
        public static implicit operator Transform(ValueTransform argValueA)
        {
            return argValueA.value;
        }
    }
}