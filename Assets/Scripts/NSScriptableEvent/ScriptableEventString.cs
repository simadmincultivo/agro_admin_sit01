using UnityEngine;

namespace NSScriptableEvent
{
    [CreateAssetMenu(fileName = "Event string", menuName = "NSScriptableEvent/String", order = 0)]
    public class ScriptableEventString : AbstractScriptableEvent<string>
    {
        
    }
}