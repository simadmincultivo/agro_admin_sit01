using UnityEngine;

namespace NSScriptableEvent
{
    [CreateAssetMenu(fileName = "Event Transform", menuName = "NSScriptableEvent/Transform", order = 0)]
    public class ScriptableEventTransform : AbstractScriptableEvent<Transform>
    {
        
    }
}