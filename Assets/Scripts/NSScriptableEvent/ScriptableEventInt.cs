using UnityEngine;

namespace NSScriptableEvent
{
    [CreateAssetMenu(fileName = "Event int", menuName = "NSScriptableEvent/Int", order = 0)]
    public class ScriptableEventInt : AbstractScriptableEvent<int>
    {
        
    }
}