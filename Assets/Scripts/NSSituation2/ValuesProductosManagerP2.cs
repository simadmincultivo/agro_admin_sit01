﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValuesProductosManagerP2 : MonoBehaviour
{   
    [Header("Precios de productos")]
    public int precioAbono1;
    public int precioAbono2;
    public int precioAbono3;
    public int precioMateriaOrganica;
    public int precioInsecticida1;
    public int precioInsecticida2;
    public int precioFungicida1;
    public int precioHerbicida1;
    public int precioHerbicida2;
    public int precioTijerasGr;
    public int precioTijerasPq;
    public int precioEstaca;
    public int precioRolloCabuya;
    public int precioAgua;
    public int precioJornal;
    public int precioPlanta;

    [System.NonSerialized] public float areaCultivo;
    [System.NonSerialized] public float cantidadPlantasDensidad;
    public int frecuenciaFertilization;
    public int frecuenciaPlagasEnfermedades;
    public int frecuenciaMalezas;
    public int frecuenciaTutorizado;
    public int frecuenciaPodasFormacion;
    public int frecuenciaPodasSanitarias;
    public int frecuenciaRiego;
    
    [Header("Valores y variables aleatorios de practica 2")]
    public float largoCultivo;
    public float anchoCultivo;
    public float distanciasDeSurcoCalleMayorValue;
    public float distanciasDeSurcoPlantasValue;
    
    private float[] distanciasDeSurcoCalleMayor = new float[] {1f, 1.1f, 1.2f, 1.3f, 1.4f};
    private float[] distanciasDeSurcoPlantas = new float[] {25.5f, 26f, 26.5f, 27f, 27.5f, 28f, 28.5f, 29f, 29.5f, 30f, 30.5f, 31f, 31.5f, 32f, 32.5f, 33f, 33.5f, 34f, 34.5f};

    void Start()
    {
        CalculateArableArea();
    }

    void CalculateArableArea()
    {
        largoCultivo = Random.Range(30, 41);
        anchoCultivo = Random.Range(6, 13);
        distanciasDeSurcoCalleMayorValue = distanciasDeSurcoCalleMayor[Random.Range(0, distanciasDeSurcoCalleMayor.Length)];
        distanciasDeSurcoPlantasValue = distanciasDeSurcoPlantas[Random.Range(0, distanciasDeSurcoPlantas.Length)];

        areaCultivo = largoCultivo * anchoCultivo;
        cantidadPlantasDensidad = (areaCultivo / (distanciasDeSurcoCalleMayorValue * distanciasDeSurcoPlantasValue));
    }
}